import { Module } from '@nestjs/common';
import { JwtModule } from '@nestjs/jwt';
import {
  ClientProxyFactory,
  ClientsModule,
  Transport,
} from '@nestjs/microservices';
import { PassportModule } from '@nestjs/passport';
import { ConfigService } from 'src/config.service';
import { AuthController } from './auth.controller';
import { AuthService } from './auth.service';
import { jwtConstants } from './constants';
import { JwtAccessTokenStrategy } from './jwt-access-token.strategy';
import { JwtRefreshTokenStrategy } from './jwt-refresh-token.strategy';
import { LocalStrategy } from './local.stragy';
import { PermissionsGuard } from './permissions/permissions.guard';
import { RolesGuard } from './roles/roles.guard';
import { SystemPermissionsGuard } from './system-permissions/system-permissions.guard';

@Module({
  imports: [
    PassportModule,
    ClientsModule.register([
      {
        name: 'AUTH_SERVICE',
        transport: Transport.TCP,
        options: {
          host: 'localhost',
          port: 3001,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'USER_SERVICE',
        transport: Transport.TCP,
        options: {
          host: 'localhost',
          port: 3003,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'PAYROLL_SERVICE',
        transport: Transport.TCP,
        options: {
          host: 'localhost',
          port: 3011,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'NOTIFICATION_SERVICE',
        transport: Transport.TCP,
        options: {
          host: 'localhost',
          port: 3004,
        },
      },
    ]),
    ClientsModule.register([
      {
        name: 'GENERAL_SERVICE',
        transport: Transport.TCP,
        options: {
          host: 'localhost',
          port: 3005,
        },
      },
    ]),
  ],
  providers: [
    ConfigService,
    AuthService,
    JwtAccessTokenStrategy,
    JwtRefreshTokenStrategy,
    LocalStrategy,
    RolesGuard,
    SystemPermissionsGuard,
    PermissionsGuard,
  ],
  controllers: [AuthController],
})
export class AuthModule { }
