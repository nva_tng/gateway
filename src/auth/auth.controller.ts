import {
  Controller,
  Post,
  Put,
  Get,
  Body,
  Req,
  Res,
  Inject,
  HttpStatus,
  HttpException,
  Request,
  Param,
  UseGuards,
  Delete,
  Query,
} from '@nestjs/common';
import { ApiTags, ApiOkResponse, ApiCreatedResponse } from '@nestjs/swagger';

import { I18nLang, I18nService } from 'nestjs-i18n';
import { LocalAuthGuard } from './local-auth.guard';
import { JwtAccessTokenGuard } from './jwt-access-token.guard';
import { AuthService } from './auth.service';
import { AuthGuard } from '@nestjs/passport';
import { JwtRefreshTokenGuard } from './jwt-refresh-token.guard';
import { ClientProxy, JsonSocket } from '@nestjs/microservices';
import { query, Response } from 'express';
import { IGateWayResponse } from 'src/interfaces/gateway-response.interface';
import { Permission } from "src/auth/permissions/permission.enum";
import { Permissions } from "src/auth/permissions/permissions.decorator";
import { PermissionsGuard } from "src/auth/permissions/permissions.guard";
import { Role } from './roles/role.enum';
import { Roles } from './roles/roles.decorator';
import { IGateWayLog } from 'src/interfaces/gateway-log.interfaces';

@Controller('auth')
@ApiTags('auth')
export class AuthController {
  gateway: string;
  logServiceUrl: string;
  controller: string;
  constructor(
    private readonly service: AuthService,
    @Inject('USER_SERVICE')
    private readonly userServiceClient: ClientProxy,
    private readonly i18n: I18nService,
    @Inject('NOTIFICATION_SERVICE')
    private readonly notifyServiceClient: ClientProxy,
  ) {
    this.gateway = 'central-gateway';
    this.controller = 'auth';
    this.logServiceUrl = `http://${process.env.LOG_SERVICE_HOST}:${process.env.LOG_SERVICE_PORT}`;
  }

  @UseGuards(LocalAuthGuard)
  @Post('/login')
  async login(@Request() req, @Res() res: Response, @I18nLang() lang: string) {
    const statusCode = parseInt(req.user.status);
    req.user.message = await this.i18n.translate('auth.' + req.user.message, {
      lang: lang,
    });
    return res.status(statusCode).json(req.user);
  }
  @UseGuards(JwtRefreshTokenGuard)
  @Get('/access-token')
  getAccessToken(@Request() req, @Res() res: Response) {
    const statusCode = parseInt(req.user.status);
    return res.status(statusCode).json(req.user);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Get('/profile')
  async getProfile(@Request() req, @Res() res: Response) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    if (!req.user.data) {
      response.message = 'ERROR';
      return res.status(HttpStatus.BAD_REQUEST).json(response);
    }
    const user = req.user.data;
    let clubIds = [];
    if (user.accountType == 'EMPLOYEE') {
      const employeeInfo = await this.userServiceClient.send('get-employee-info', { accountId: user.accountId }).toPromise();
      if (employeeInfo) {
        clubIds.push(employeeInfo.clubId);
      }
    }
    response.data = {
      ...user,
      clubIds
    };
    return res.status(response.status).json(response);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Post('/add-fcm-token')
  public async addFCMToken(
    @Request() req,
    @Body() data: any,
    @Res() res: Response,
  ) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    if (!req.user.data) {
      response.message = 'ERROR';
      return res.status(HttpStatus.BAD_REQUEST).json(response);
    }
    const user = req.user.data;
    data.accountId = user.accountId;
    const result = await this.service.addFCMToken(data);
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Delete('/remove-fcm-token')
  public async removeFCMToken(
    @Request() req,
    @Body() data: any,
    @Res() res: Response,
  ) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    if (!req.user.data) {
      response.message = 'ERROR';
      return res.status(HttpStatus.BAD_REQUEST).json(response);
    }
    const user = req.user.data;
    data.accountId = user.accountId;
    const result = await this.service.removeFCMToken(data);
    return res.status(result.status).json(result);
  }
  @Post('/register-account')
  public async createAccount(
    @Request() req,
    @I18nLang() language: string,
    @Body() data: any,
    @Res() res: Response,
  ) {
    const result = await this.service.createAccount(data, language);
    const gatewayLog: IGateWayLog = {
      name: this.gateway,
      method: 'POST',
      route: `${this.controller}/register-account`,
      accountId: req?.user?.data?.accountId,
      body: data,
      params: null,
      query: null,
      statusCode: result.status,
      data: result.data,
      message: result.message,
      errors: result.errors
    }
    try {
      this.service.post(gatewayLog, this.logServiceUrl, '/log/gateway');
    } catch (err) {
    }
    return res.status(result.status).json(result);
  }
  @Delete('/account/:accountId')
  public async deleteMemberAccount(
    @Request() req,
    @I18nLang() language: string,
    @Param() params: any,
    @Res() res: Response,
  ) {
    const result = await this.service.deleteMemberAccount({ ...params });
    const gatewayLog: IGateWayLog = {
      name: this.gateway,
      method: 'DELETE',
      route: `${this.controller}/account/:accountId`,
      accountId: req?.user?.data?.accountId,
      body: null,
      params,
      query: null,
      statusCode: result.status,
      data: result.data,
      message: result.message,
      errors: result.errors
    }
    try {
      this.service.post(gatewayLog, this.logServiceUrl, '/log/gateway');
    } catch (err) {
    }
    return res.status(result.status).json(result);
  }
  @Post('/activate')
  public async activateAccount(
    @Body() data: any,
    @Res() res: Response,
    @I18nLang() language: string,
  ) {
    const result = await this.service.activateAccount(data);
    result.message = await this.i18n.translate('auth.' + result.message, {
      lang: language,
    });
    return res.status(result.status).json(result);
  }
  @Post('/check-otp')
  public async checkOTP(
    @Body() data: any,
    @Res() res: Response,
    @I18nLang() language: string,
  ) {
    const result = await this.service.checkOTP(data);
    result.message = await this.i18n.translate('auth.' + result.message, {
      lang: language,
    });
    return res.status(result.status).json(result);
  }
  @Post('activate/resend-code')
  public async resendActivateCode(
    @I18nLang() language: string,
    @Body() data: any,
    @Res() res: Response,
  ) {
    const result = await this.service.resendActivateCode(data, language);
    result.message = await this.i18n.translate('auth.' + result.message, {
      lang: language,
    });
    return res.status(result.status).json(result);
  }
  @Post('/set-password')
  public async setPassword(@Body() data: any, @Res() res: Response) {
    const result = await this.service.setPassword(data);
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Post('/change-password')
  public async changePassword(
    @Request() req,
    @Body() data: any,
    @Res() res: Response,
  ) {
    const accountId = req.user.data.accountId;
    const result = await this.service.changePassword({ ...data, accountId });
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Put('/profile')
  public async updateMemberProfile(
    @Request() req,
    @Body() data: any,
    @Res() res: Response,
  ) {
    const result = await this.service.updateProfile({ ...data });
    const gatewayLog: IGateWayLog = {
      name: this.gateway,
      method: 'PUT',
      route: `${this.controller}/profile`,
      accountId: req?.user?.data?.accountId,
      body: data,
      params: null,
      query: null,
      statusCode: result.status,
      data: result.data,
      message: result.message,
      errors: result.errors
    }
    try {
      this.service.post(gatewayLog, this.logServiceUrl, '/log/gateway');
    } catch (err) {
    }
    return res.status(result.status).json(result);
  }
  @Post('/check-access-right')
  public async checkAccessRight(@Body() body) {
    return await this.service.checkAccessRight(body);
  }
  @Post('/write-access-log')
  public async writeAccessLog(@Body() body) {
    return await this.service.writeAccessLog(body);
  }
  @Post('update-fingerprint')
  public async updateFingerprint(@Body() body) {
    return await this.service.updateFingerprint(body);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Post('/account-setting')
  public async updateAccountSetting(
    @Request() req,
    @Body() data: any,
    @Res() res: Response,
  ) {
    const accountId = req.user.data.accountId;
    const result = await this.service.updateAccountSetting({
      ...data,
      accountId,
    });
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Get('/account-setting')
  public async getAccountSetting(
    @Request() req,
    @Query() query: any,
    @Res() res: Response,
  ) {
    const accountId = req.user.data.accountId;
    const result = await this.service.getAccountSetting({
      ...query,
      accountId,
    });
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Get('/role')
  public async getListRoles(
    @Request() req,
    @Query() query: any,
    @Res() res: Response,
  ) {
    const result = await this.service.getListRoles({
    });
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Get('/role-permission/:roleId')
  public async getListRolePermissions(
    @Request() req,
    @Param() params: any,
    @Res() res: Response,
  ) {
    const result = await this.service.getListRolePermissions({
      ...params
    });
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Put('/role-permission/:roleId')
  public async updateRolePermissions(
    @Request() req,
    @Param() params: any,
    @Body() body,
    @Res() res: Response,
  ) {
    const result = await this.service.updateRolePermissions({
      ...params,
      ...body
    });
    const gatewayLog: IGateWayLog = {
      name: this.gateway,
      method: 'PUT',
      route: `${this.controller}/role-permission/:roleId`,
      accountId: req?.user?.data?.accountId,
      body,
      params,
      query: null,
      statusCode: result.status,
      data: result.data,
      message: result.message,
      errors: result.errors
    }
    try {
      this.service.post(gatewayLog, this.logServiceUrl, '/log/gateway');
    } catch (err) {
    }
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Put('/role/:roleId')
  public async updateRole(
    @Request() req,
    @Param() params: any,
    @Body() body,
    @Res() res: Response,
  ) {
    const result = await this.service.updateRole(params.roleId, {
      ...body
    });
    const gatewayLog: IGateWayLog = {
      name: this.gateway,
      method: 'PUT',
      route: `${this.controller}/role/:roleId`,
      accountId: req?.user?.data?.accountId,
      body,
      params,
      query: null,
      statusCode: result.status,
      data: result.data,
      message: result.message,
      errors: result.errors
    }
    try {
      this.service.post(gatewayLog, this.logServiceUrl, '/log/gateway');
    } catch (err) {
    }
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Post('/role')
  public async createRole(
    @Request() req,
    @Param() params: any,
    @Body() body,
    @Res() res: Response,
  ) {
    const result = await this.service.createRole(body);
    const gatewayLog: IGateWayLog = {
      name: this.gateway,
      method: 'POST',
      route: `${this.controller}/role`,
      accountId: req?.user?.data?.accountId,
      body,
      params,
      query: null,
      statusCode: result.status,
      data: result.data,
      message: result.message,
      errors: result.errors
    }
    try {
      this.service.post(gatewayLog, this.logServiceUrl, '/log/gateway');
    } catch (err) {
    }
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Get('/permission')
  public async getListPermissions(
    @Request() req,
    @Query() query: any,
    @Res() res: Response,
  ) {
    const result = await this.service.getListPermissions({
    });
    return res.status(result.status).json(result);
  }
  @UseGuards(JwtAccessTokenGuard)
  @Get('/system-permission')
  public async getListSystemPermissions(
    @Request() req,
    @Query() query: any,
    @Res() res: Response,
  ) {
    const result = await this.service.getListSystemPermissions({
    });
    return res.status(result.status).json(result);
  }


}
