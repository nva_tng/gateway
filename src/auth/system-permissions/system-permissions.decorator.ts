import { SetMetadata } from '@nestjs/common';
import { SystemPermission } from './system-permission.enum';

export const SYSTEM_PERMISSION_KEY = 'systemPermissions';
export const SystemPermissions = (...systemPermissions: SystemPermission[]) => SetMetadata(SYSTEM_PERMISSION_KEY, systemPermissions);