export enum SystemPermission {
    CRM = 'CRM',
    POS = 'POS',
    ADMIN = 'ADMIN',
}