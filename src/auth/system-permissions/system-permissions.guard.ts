import { Injectable, CanActivate, ExecutionContext } from '@nestjs/common';
import { Reflector } from '@nestjs/core';
import { SystemPermission } from './system-permission.enum';
import { SYSTEM_PERMISSION_KEY } from './system-permissions.decorator';

@Injectable()
export class SystemPermissionsGuard implements CanActivate {
  constructor(private reflector: Reflector) { }

  canActivate(context: ExecutionContext): boolean {
    const requiredPermissions = this.reflector.getAllAndOverride<SystemPermission[]>(SYSTEM_PERMISSION_KEY, [
      context.getHandler(),
      context.getClass(),
    ]);
    if (!requiredPermissions) {
      return true;
    }
    const { user } = context.switchToHttp().getRequest();
    return requiredPermissions.some((systemPermissions) => user.data.systemPermissions?.includes(systemPermissions));
  }
}