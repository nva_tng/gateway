import { HttpStatus, Inject, Injectable } from '@nestjs/common';
import { ClientProxy } from '@nestjs/microservices';
import { resolveSoa } from 'dns';
import { I18nService } from 'nestjs-i18n';
import { IGateWayResponse } from 'src/interfaces/gateway-response.interface';
import { IServiceResponse } from 'src/interfaces/service-response.interface';
import fetch from 'node-fetch';
import * as moment from 'moment';
@Injectable()
export class AuthService {
  constructor(
    @Inject('AUTH_SERVICE')
    private readonly authServiceClient: ClientProxy,
    @Inject('USER_SERVICE')
    private readonly userServiceClient: ClientProxy,
    @Inject('PAYROLL_SERVICE')
    private readonly payrollServiceClient: ClientProxy,
    @Inject('NOTIFICATION_SERVICE')
    private readonly notifyServiceClient: ClientProxy,
    @Inject('GENERAL_SERVICE')
    private readonly generalServiceClient: ClientProxy,
    private readonly i18n: I18nService,
  ) { }

  public post(body: any, address: string, path: string) {
    const url = new URL(path, address);
    // console.log(body);
    // TODO: Need the authentication
    return fetch(url.toString(), {
      method: 'post',
      body: JSON.stringify(body),
      headers: {
        'Content-Type': 'application/json',
      },
    }).then(res => {
      if (res.status < 200 || res.status > 302) {
        console.log(`response status text: ${res.status} ${res.statusText}`);
        return null;
      }
      return res.json();
    });
  }

  private generateRandomCode(length: number) {
    let text = '';
    const possible = '0123456789QWERTYUIOPASDFGHJKLZXCVBNMqwertyuiopasdfghjklzxcvbnm';
    for (let i = 0; i < length; i++) {
      text += possible.charAt(Math.floor(Math.random() * possible.length));
    }
    return text;
  }

  public async login(username: string, password: string): Promise<any> {
    const user = await this.authServiceClient
      .send('login', { username, password })
      .toPromise();
    return user;
  }
  public async resendActivateCode(data: any, language: string) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const generateCodeResponse = await this.authServiceClient
      .send('generate-code', data)
      .toPromise();
    if (generateCodeResponse.status !== HttpStatus.OK) {
      response.status = HttpStatus.BAD_REQUEST;
      response.message = 'ACCOUNT_NOT_FOUND';
      return response;
    }
    const user = await this.authServiceClient
      .send('get-profile', { accountId: generateCodeResponse?.data.accountId })
      .toPromise();
    const notifyDTO = {
      type: data.type,
      phone: generateCodeResponse.data.phoneNumber,
      email: generateCodeResponse.data.email,
      settings: user?.data?.settings,
      params: [generateCodeResponse.data.activateCode],
      language: language,
      isRealTime: true,
    };
    this.notifyServiceClient.send('send-notification', notifyDTO).toPromise();
    response.data = {
      accountId: generateCodeResponse.data.accountId,
      resendAt: new Date(),
    };
    return response;
  }

  public async getProfile(data: any): Promise<IServiceResponse> {
    const profileResponse = await this.authServiceClient
      .send('get-profile', data)
      .toPromise();
    return profileResponse;
  }
  public async updateProfile(data: any): Promise<IServiceResponse> {
    const profileResponse = await this.authServiceClient
      .send('update-member-profile', data)
      .toPromise();
    return profileResponse;
  }
  public async updateAccountSetting(data: any) {
    const settingResponse = await this.authServiceClient
      .send('update-account-setting', data)
      .toPromise();
    return settingResponse;
  }
  public async getAccountSetting(data: any) {
    const settingResponse = await this.authServiceClient
      .send('get-account-setting', {
        accountId: data.accountId,
        accountSettingKey: data.key,
      })
      .toPromise();
    return settingResponse;
  }

  public async addFCMToken(data: any): Promise<IServiceResponse> {
    const fcmTokenResponse = await this.authServiceClient
      .send('add-fcm-token', data)
      .toPromise();
    return fcmTokenResponse;
  }
  public async removeFCMToken(data: any) {
    const fcmTokenResponse = await this.authServiceClient
      .send('remove-fcm-token', data)
      .toPromise();
    return fcmTokenResponse;
  }
  public async getFCMTokenByAccountId(data: any): Promise<IServiceResponse> {
    const fcmTokenResponse = await this.authServiceClient
      .send('get-fcm-token', data)
      .toPromise();
    return fcmTokenResponse;
  }
  public async renewAccessToken(data: any) {
    const renewAccessTokenResponse = await this.authServiceClient
      .send('renew-access-token', data)
      .toPromise();
    return renewAccessTokenResponse;
  }
  public async deleteMemberAccount(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const memberInfo = await this.userServiceClient.send('get-member-info', { accountId: data.accountId }).toPromise();
    const existedContract = await this.userServiceClient.send('get-member-contract', { memberId: memberInfo, deletedAt: null }).toPromise();
    if (existedContract) {
      response.status = HttpStatus.BAD_REQUEST;
      response.message = "EXISTED_CONTRACT";
      return response;
    }
    const deleteMemberAccount: IServiceResponse = await this.authServiceClient
      .send('delete-account', data)
      .toPromise();
    response = deleteMemberAccount;
    return response;
  }
  public async createPTorEmployeeAccount(data: any, language: string) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const createAccountResponse: IServiceResponse = await this.authServiceClient
      .send('register-pt-employee-account', data)
      .toPromise();
    const createPTInfoResponse: IServiceResponse = await this.userServiceClient
      .send('create-pt-info', {
        accountId: createAccountResponse?.data?.accountId,
        clubId: data.clubId,
        infoEn: data.infoEn,
        infoVi: data.infoVi,
      })
      .toPromise();
    console.log(
      'AuthService -> createPTAccount -> createPTInfoResponse',
      createPTInfoResponse,
    );
    if (!createAccountResponse || !createPTInfoResponse) {
      response.status = HttpStatus.BAD_REQUEST;
      response.message = 'CREATED_FAILED';
    }
    response.status = createAccountResponse.status;
    response.data = createAccountResponse.data;
    response.message = createAccountResponse.message;
    return response;
  }
  public async createAccount(data: any, language: string) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    let body = {
      ...data,
      language,
    };
    const password = this.generateRandomCode(9);
    if (data.accountType !== 'MEMBER') {
      body = {
        ...body,
        password,
      }
    }
    if (data.accountType == 'PT') {
      body = {
        ...body,
        roleId: 2,
      }
    }
    const createAccountResponse: IServiceResponse = await this.authServiceClient
      .send('register-account', body)
      .toPromise();
    if (!createAccountResponse.data) {
      response.status = createAccountResponse.status;
      response.message = await this.i18n.translate(
        'auth.' + createAccountResponse.message,
        { lang: language },
      );
      return response;
    }
    const accountId = createAccountResponse.data.accountId;
    if (data.accountType == 'EMPLOYEE') {
      await this.userServiceClient.send('create-employee-info', { accountId, clubIds: data.clubIds }).toPromise();
    }
    if (data.accountType == 'PT') {
      const ptInfo = await this.userServiceClient.send('create-pt-info', { accountId, isInstructor: data.isInstructor, clubIds: data.clubIds }).toPromise();
      await this.payrollServiceClient.send('create-pt-revnue-history', { ptId: ptInfo.data.ptId, percentage: 45, revenue: 0, bonus: 0, time: moment().startOf('month').format() }).toPromise();
    }
    //send notification
    if (data.accountType !== 'MEMBER') {
      const clubDetail = await this.generalServiceClient
        .send('get-club-detail', { clubId: data.clubIds[0] })
        .toPromise();
      const findIndex = clubDetail.findIndex(
        item => item.detailKey == 'ip_address',
      );
      this.post(
        { accountId: createAccountResponse.data.accountId, pin: createAccountResponse.data.pin },
        `http://${clubDetail[findIndex].detailValueEn}:3031`,
        '/reg-new-member',
      );
      const user = await this.authServiceClient
        .send('get-profile', { accountId })
        .toPromise();
      const notifyDTO = {
        type: 'FIRST_PASSWORD',
        accountId: accountId,
        phone: createAccountResponse.data.phoneNumber,
        settings: user?.data?.settings,
        email: createAccountResponse.data.email,
        params: [
          password
        ],
        language: data?.language || 'vi',
        isRealTime: true,
      };
      try {
        this.notifyServiceClient.send('send-notification', notifyDTO).toPromise();
      } catch (err) {
      }
    }
    response.data = createAccountResponse.data;
    return response;
  }
  public async activateAccount(data: any) {
    const activeResponse = await this.authServiceClient
      .send('activate', data)
      .toPromise();
    return activeResponse;
  }
  public async checkOTP(data: any) {
    const activeResponse = await this.authServiceClient
      .send('check-otp', data)
      .toPromise();
    return activeResponse;
  }
  public async setPassword(data: any) {
    const activeResponse = await this.authServiceClient
      .send('set-password', data)
      .toPromise();
    return activeResponse;
  }
  public async changePassword(data: any) {
    const changePwRes = await this.authServiceClient
      .send('change-password', data)
      .toPromise();
    return changePwRes;
  }
  private async checkMemberAccessRight(data: any) {
    let memberContractResponse,
      isAllowAccess = false;
    const memberInfoResponse = await this.userServiceClient
      .send('get-member-info', { accountId: data.accountId })
      .toPromise();
    if (memberInfoResponse) {
      memberContractResponse = await this.userServiceClient
        .send('get-member-contract', {
          memberId: parseInt(memberInfoResponse?.id),
          status: 'CURRENT',
          isFrozen: false,
        })
        .toPromise();
      if (!memberContractResponse) {
        return isAllowAccess;
      }
      if (parseInt(data.clubId) === parseInt(memberContractResponse.clubId)) {
        isAllowAccess = true;
      }
    }
    return isAllowAccess;
  }
  public async checkAccessRight(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const profileResponse = await this.authServiceClient
      .send('get-profile', data)
      .toPromise();
    //check account access right
    let isAllowAccess = false;
    if (profileResponse?.data?.accountType === 'MEMBER') {
      //check member access right
      isAllowAccess = await this.checkMemberAccessRight(data);
    }
    response.data = { isAllowAccess };
    return response;
  }
  public async writeAccessLog(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const writeAccessLog = await this.authServiceClient
      .send('write-access-log', data)
      .toPromise();
    if (!writeAccessLog) {
      response.status = HttpStatus.BAD_REQUEST;
      return response;
    }
    response.data = writeAccessLog;
    return response;
  }
  public async updateFingerprint(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const updateFingerprint = await this.authServiceClient
      .send('update-fingerprint', data)
      .toPromise();
    if (!updateFingerprint) {
      response.status = HttpStatus.BAD_REQUEST;
      return response;
    }
    response.data = updateFingerprint;
    return response;
  }
  public async getListRoles(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const listRoles = await this.authServiceClient.send('get-list-roles', {}).toPromise();
    response.data = listRoles
    return response;
  }
  public async getListPermissions(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const listPermissionGroup = await this.authServiceClient.send('get-list-permission-groups', {}).toPromise();
    const listPermissions = await Promise.all(listPermissionGroup.map(async group => {
      let permissions = await this.authServiceClient.send('get-list-permissions', { permissionGroupId: group.id }).toPromise();
      return {
        ...group,
        permissions
      }
    }))
    response.data = listPermissions
    return response;
  }
  public async getListSystemPermissions(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const listSystemPermissionGroup = await this.authServiceClient.send('get-list-system-permissions', {}).toPromise();
    response.data = listSystemPermissionGroup
    return response;
  }
  public async getListRolePermissions(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const listRolePermissions = await this.authServiceClient.send('get-list-role-permissions', { roleId: data.roleId }).toPromise();
    const listRoleSystemPermissions = await this.authServiceClient.send('get-list-role-system-permissions', { roleId: data.roleId }).toPromise();
    let permissions = [], systemPermissions = [];
    permissions = await Promise.all(listRolePermissions.map(async role => {
      const permission = await this.authServiceClient.send('get-permission', { id: role.permissionId }).toPromise();
      return permission
    }))
    systemPermissions = await Promise.all(listRoleSystemPermissions.map(async role => {
      const permission = await this.authServiceClient.send('get-system-permission', { id: role.systemPermissionId }).toPromise();
      return permission
    }))
    response.data = { permissions, systemPermissions };
    return response;
  }
  public async updateRolePermissions(data: any) {
    const response: IServiceResponse = await this.authServiceClient.send('update-role-permissions', data).toPromise();
    return response;
  }
  public async updateRole(id: any, data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const result = await this.authServiceClient.send('update-role', { id, dto: data }).toPromise();
    if (result?.raw.affectedRows == 0) {
      response.status = HttpStatus.BAD_REQUEST;
      response.message = 'UPDATE_FAIL';
      return response;
    }
    response.message = "ROLE_UPDATED"
    return response;
  }
  public async createRole(data: any) {
    let response: IGateWayResponse = {
      status: HttpStatus.OK,
      data: null,
      message: null,
      errors: null,
    };
    const result = await this.authServiceClient.send('create-role', data).toPromise();
    response.data = result
    response.message = "ROLE_CREATED"
    return response;
  }
}
