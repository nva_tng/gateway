import { Transport } from '@nestjs/microservices';

export class ConfigService {
    private readonly envConfig: { [key: string]: any } = null;

    constructor() {
        this.envConfig = {};
        this.envConfig.port = process.env.IOT_GATEWAY_PORT || 3030;
        this.envConfig.authService = {
            options: {
                port: process.env.AUTH_SERVICE_PORT,
                host: process.env.AUTH_SERVICE_HOST,
            },
            transport: Transport.TCP,
        };
        this.envConfig.notifyService = {
            options: {
                port: process.env.NOTIFICATION_SERVICE_PORT,
                host: process.env.NOTIFICATION_SERVICE_HOST,
            },
            transport: Transport.TCP,
        };
    }

    get(key: string): any {
        return this.envConfig[key];
    }
}
