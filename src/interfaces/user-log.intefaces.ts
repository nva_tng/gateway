export interface IUserLog {
    gatewayName: string;
    method: string;
    accountId: string;
    action: string | null;
    createdBy: string | null;
}