export interface IServiceLog {
    serviceName: string;
    gatewayName: string;
    message: string;
    body: object | null;
    data: object | null;
}
