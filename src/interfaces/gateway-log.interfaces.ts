export interface IGateWayLog {
    name: string;
    method: string;
    route: string;
    accountId: string;
    body: object | null;
    params: object | null;
    query: object | null;
    statusCode: number | 200;
    data: object | null;
    message: string | null;
    errors: { [key: string]: any } | null;
}
