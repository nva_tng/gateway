import { INestMicroservice } from '@nestjs/common';
import { NestFactory } from '@nestjs/core';
import { MicroserviceOptions, Transport } from '@nestjs/microservices';
import { NestExpressApplication } from '@nestjs/platform-express';
import { SwaggerModule, DocumentBuilder } from '@nestjs/swagger';
import { AppModule } from './app.module';
import { ConfigService } from './config.service';

async function bootstrap() {
  const app = await NestFactory.create<NestExpressApplication>(AppModule);
  // var whitelist = ['http://localhost'];
  app.enableCors({
    // origin: function (origin, callback) {
    //   if (whitelist.indexOf(origin) !== -1) {
    //     console.log("allowed cors for:", origin)
    //     callback(null, true)
    //   } else {
    //     console.log("blocked cors for:", origin)
    //     callback(new Error('Not allowed by CORS'))
    //   }
    // },
    origin: true,
    allowedHeaders:
      'X-Requested-With, X-HTTP-Method-Override, Content-Type, Accept, Accept-Language, Authorization,',
    methods: 'GET,PUT,POST,DELETE,UPDATE,OPTIONS',
    credentials: true,
  });
  const aedes = require('aedes')()
  const server = require('net').createServer(aedes.handle)
  const port = 1883

  server.listen(port, function () {
    console.log('server started and listening on port ', port)
  })
  server.on('ready', () => {
    server.emit('test', { data: 'das' })
  })
  await app.listen(new ConfigService().get('port'));
}
bootstrap();
